# Housie Ticket Generator. #

Developer- Tarun Tej Velaga. 
Date - 08/19/2016.

* Open "housie.html" to generate Housie Tickets. 
* Click on regenerate to Re- Generate another set of tickets. 
* Click on Print to take a print of the generated tickets. 

This page uses javascript for generating the tickets. (Defaulted to 12)
Feel free to use it.